# JAVA-27
Scope variable

# Before correct this question
1. Fork this project by click fork button in this repository and set to plublic.
2. Clone project from your repository and correct it.
3. After correct it, submit your git url.

# Database
1. Download and install (Skip this step if you already have docker)
    >> https://www.docker.com/products/docker-desktop

2. Pull image mysql using command below (Skip this step if you already have mysql image)
    >> docker pull mysql

    2.1 If you use M1 processor uncomment line 5 in docker-compose.yml file.
    
    2.2 Check image in docker, if you got an image name mysql with tag didn't match latest please update line 6 from mysql to mysql:{tag_you_got} in docker-compose.yml file.

3. Open terminal and go to JAVA-27 folder

4. Stop docker database earlier using command below
    >> docker-compose down -v

5. Start docker database using command below
    >> docker-compose up -d

6. If you would like to stop docker database using command below
    >> docker-compose down -v

# Postman
https://www.getpostman.com/collections/4c0e437a752516f9e9b8

# Connect from database tool
SERVER_HOST: localhost
PORT: 3306
DATABASE: PRIOR_GAME_DB
USERNAME: dev
PASSWORD: dev
CREATE TABLE `PRIOR_GAME_DB`.student
(
    student_id INTEGER     NOT NULL AUTO_INCREMENT,
    first_name  VARCHAR(50) NOT NULL,
    last_name   VARCHAR(50),
    gender_code INTEGER,
    gender      VARCHAR(10),
    PRIMARY KEY (student_id)
);
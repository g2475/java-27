package org.prior.game.scopevariable.service;

import org.prior.game.scopevariable.bean.DescriptionDetail;
import org.prior.game.scopevariable.entity.Student;
import org.prior.game.scopevariable.repo.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    DescriptionService descriptionService;

    public List<Student> updateMaleStudents() {
        List<Student> result = new ArrayList<>();
        List<Student> studentList = studentRepository.findAll();
        DescriptionDetail descriptionDetail = null;
        for(Student s : studentList) {
            try {
                if(1 == s.getGenderCode()) {
                    descriptionDetail = descriptionService.getDescriptionDetail(s.getGenderCode());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if(descriptionDetail!=null){
                    s.setGender(descriptionDetail.getGenderDescription());
                }
                studentRepository.save(s);
                result.add(s);
            }
        }
        return result;
    }

    public List<Student> resetData() {
        List<Student> result = new ArrayList<>();
        List<Student> studentList = studentRepository.findAll();
        studentList.stream().forEach(student -> {
            if(1==student.getGenderCode()){
                student.setGender(null);
            } else {
                student.setGender("FEMALE");
            }
            studentRepository.save(student);
            result.add(student);
        });
        return result;
    }
}

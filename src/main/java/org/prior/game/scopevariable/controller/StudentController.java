package org.prior.game.scopevariable.controller;

import org.prior.game.scopevariable.entity.Student;
import org.prior.game.scopevariable.repo.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.prior.game.scopevariable.service.StudentService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;
    private final StudentRepository studentRepository;

    @PostMapping("/students")
    public List<Student> getStudents() {
        return studentRepository.findAll();
    }

    @PostMapping("/updateMaleStudents")
    public List<Student> getResult() {
        return studentService.updateMaleStudents();
    }

    @PostMapping("/resetData")
    public List<Student> resetData() {
        return studentService.resetData();
    }
}
